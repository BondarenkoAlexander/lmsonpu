from PIL import Image
from django.db import models
from django.contrib.auth.models import AbstractUser

from accounts.validators import validate_phone_number


class User(AbstractUser):
    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    surname = models.CharField(max_length=210, blank=True)
    phone_number = models.CharField(null=False,
                                    max_length=20,
                                    validators=[validate_phone_number],
                                    default="+38(098)000-0000")
    image = models.ImageField(default='default.png', upload_to='accounts')

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        image = Image.open(self.image)
        image.thumbnail((300, 300), Image.ANTIALIAS)
        image.save(self.image.path)
